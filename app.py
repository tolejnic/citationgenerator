# -*- coding: utf-8 -*-

from flask import Flask
from flask import render_template
from flask import request
from habanero import Crossref
from habanero import cn

app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    citation = ""
    return render_template('index.html', citation=citation)


@app.route('/citation_generator', methods=['POST'])
def citation_generator():
    data = request.json
    citations = []
    if data:
        style = data['style'].lower()
        dois = data['doi'].strip("\n").split("\n")
        for doi in dois:
            try:
                citation = cn.content_negotiation(ids=doi, format="text", style=style)
            except:
                cr = Crossref()
                citation = cr.works(query=doi, rows=1)
                doi = citation['message']['items'][0]['DOI']
                citation = cn.content_negotiation(ids=doi, format="text", style=style)

            citations.append(citation.replace("â", "-").replace('â', '"').replace('â', '"'))

    return '<br /><br />'.join(citations)  # return list as string


if __name__ == "__main__":
    # context = ('/Users/tim/web_dev/castle/app/ssl/server.crt', '/Users/tim/web_dev/castle/app/ssl/server.key')
    app.run(debug=True, port=33507)  # , ssl_context=context)
